package inf226.inchat;

public final class UserName {
    public final String username;

    public UserName(String username){
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
