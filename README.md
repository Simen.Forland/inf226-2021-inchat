# INCHAT – The INsecure CHAT application

Welcome to this second mandatory assignment of INF226.
In this assignment you will be analysing the security
of a program called inChat – a very simple chat application,
in the shape of a [Jetty](https://www.eclipse.org/jetty/)
web application.

inChat has been especially crafted to contain a number
of security flaws. You can imagine that it has been
programmed by a less competent collegue, and that after
numerous securiy incidents, your organisation has decided
that you – a competent security professional – should take
some time to secure the app.

## Getting and building the project

Log into [`git.app.uib.no`](https://git.app.uib.no/Hakon.Gylterud/inf226-2020-inchat) and make your
own fork of the project there. *Make sure your fork is private!*
You can then clone your repo to your own machine.

To build the project you can use Maven on the command line, or configure
your IDE to use Maven to build the project.

 - `mvn compile` builds the project
 - `mvn test` runs the tests. (There are only a few unit test – feel free to add more).
 - `mvn exec:java` runs the web app.

Once the web-app is running, you can access it on [`localhost:8080`](http://localhost:8080/).

## Updates

Most likely the source code of the project will be updated by Håkon
while you are working on it. Therefore, it will be part of
your assignment to merge any new commits into your own branch.

## Improvements?

Have you found a non-security related bug?
Feel free to open an issue on the project GitLab page.
The best way is to make a separate `git branch` for these
changes, which do not contain your sulutions.

(This is ofcourse completely volountary – and not a graded
part of the assignment)

If you want to add your own features to the chat app - feel free
to do so! If you want to share them, contact Håkon and we can
incorporate them into the main repo.




## Assignment notes:

Task 0a)    
    Here I used SCrypt to encrypt the password of the user when creating an account and to decrypt
    when logging into an account. You can find my uses of SCrypt in the Account class.

Task 0b)
    Her I created the classes UserName and Password. I then replaced all uses of String username 
    and String password in the Handler and InChat with these classes. I know the task asked to
    change the Strings used in Account and User. But since these classes takes their input from
    InChat and Handler, I saw no reason to replace the ones in Account and User.
    I did fix the password check for "login" in InChat. I also created a NIST password checker
    for the "register" in InChat. It's currently commented out, as it makes the mvn test fail, since
    one of the test user has a password shorter than 8 characters. It does however work and can be 
    found at line 111.

Task 0c) 
    Here I simply added an ".setSecure(true);" to the new cookies created on line 139 in the handler.
    This was the solution reccomended by SonarQube.

Task 1) 
    SonarQube found 34 counts of SQL Injection. These were all in the different Storage classes.
    According to SonarQube, I was able to fix almost all the Injection attacks except the ones that 
    used "executeQuery(sql)" instead of "executeUpdate(sql)". When I tried to fix these, the mvn test
    would fail.
    While fixing these injections I ran the "mvn test" to make sure everything was working, however. 
    When I finished, I discovered that InChat runs into an HTTP ERROR 404 when you try to make a new
    channel. I did wasn't able to find the problem, so Create Channel is still broken. 

Task 2)
    I was only able to add an HTTP ONLY flag on the Cookies created, however. It is currently
    commented out, as I ran into a problem where I was never able to leave the create user window.
    You can see it in Handler.java at line 141

Task 3)
    I did not manage to complete this task

Task 4)
    I was able to create an "if(request.getParameter("setpermission")!= null)" in Handler which called the
    setRole in InChat when you press the set permission button. The setRole however is not complete. It 
    currently allows you to set the role of an existing user, however the roles won't save properly and
    everyone can use it despite the fact that the owner is the only one supposed to use it.
    I did however make it print out the user who's role has been changed in the form of a chat message.
    The owner is also set when creating the channel, and all new users joining is automatically set as 
    participants.
    The roles doesn't actually affect anything. I tried to make if statements similar to the ones in 
    login and register where only users in the owner or participants lists could for example post a message
    or delete a comment. This however caused a problem where when the page would crash if you posted as a
    user without permission.
    In the end. You're able to set roles and be notified when they are set, but they don't actually do anything.

Task 5)
    I did not manage to complete this task
    